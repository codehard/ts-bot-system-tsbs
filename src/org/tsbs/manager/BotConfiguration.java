package org.tsbs.manager;

import java.io.*;
import java.util.HashMap;

/**
 * project: TS-Bot-System [TSBS]
 * in package org.tsbs.manager
 * created on 21.04.2014
 *
 * @author falx
 * @version v0.1a
 */
public class BotConfiguration
{
    // -------------------------------------------------------------------------------------------------------------- //

                // ---------------------------------------------------------------------------- //
                // -- standard keys used by this configuration system as static declarations -- //
                // ---------------------------------------------------------------------------- //

    // -------------------------------------------------------------------------------------------------------------- //




    /** value of this key contains the path which contains the tsBot directory used by this manager.
     *  The tsBot directory contains at least the config.cfg where this config comes from
     */
    public static final String MAIN_BOT_DIRECTORY   = "mainPath";

    /**  value of this key contains the path to a directory where to find the external functions */
    public static final String FUNCTION_DIRECTORY   = "functionPath";

    /** value of this key contains the path to the config file where the config map is read from */
    public static final String CONFIG_FILE_PATH     = "configFile";

    /** path to the file to write the log to. If its null the logger will not log to any file */
    public static final String LOGGING_FILE_PATH    = "loggingFile";

    /** host name of the ts3 server to connect to */
    public static final String HOST_NAME            = "host";

    /** loginname used by the bot. TSQuery-account is only able to listen, to do advanced stuff use an Admin-account */
    public static final String LOGIN_NAME           = "loginName";

    /** login password used by the bot. It is not to recommended to save the password within the configuration file */
    public static final String LOGIN_PASSWORD       = "loginPassword";

    /** number of threads used by the command handler. If you don't know which number would be appropriate then don't use this field! */
    public static final String CMD_HANDLER_PROC_NO  = "cmdHandlerProcNo";




    // -------------------------------------------------------------------------------------------------------------- //

                                            // ------------------------- //
                                            // -- member declarations -- //
                                            // ------------------------- //

    // -------------------------------------------------------------------------------------------------------------- //

    private File mConfigFile;
    private BufferedReader mBReader;
    private HashMap<String, String> mConfigMap;




    // -------------------------------------------------------------------------------------------------------------- //

                                        // ----------------------------------- //
                                        // -- Constructor and Basic methods -- //
                                        // ----------------------------------- //

    // -------------------------------------------------------------------------------------------------------------- //




    /**
     *
     * @param path
     * @throws FileNotFoundException
     */
    public BotConfiguration( String path ) throws FileNotFoundException, IOException
    {
        this.mConfigFile   = new File( path );
        this.mBReader      = new BufferedReader( new FileReader( this.mConfigFile ) );
        this.mConfigMap    = new HashMap<>();
        loadConfiguration();
    }


    private void loadConfiguration() throws IOException
    {
        String s;
        while( (s = mBReader.readLine()) != null )
        {
            s.toLowerCase();
            String[] pair = s.split( ":", 2 );
            if( pair.length == 2 )
                mConfigMap.put( pair[0], pair[1] );
        }
    }


    /**
     * if the file is changed
     * @return a map containing the configuration key,value pairs
     * @throws IOException
     */
    public HashMap<String, String> getConfiguration() throws IOException
    {
        return mConfigMap;
    }


    /**
     * @return the configuration file used by this BotConfiguration
     */
    public File getConfigFile()
    {
        return mConfigFile;
    }

    /**
     * Sets the Configuration file used by this BotConfiguration to mConfigFile
     * @param mConfigFile
     * @throws FileNotFoundException
     */
    public void setConfigFile( File mConfigFile ) throws FileNotFoundException, IOException
    {
        this.mConfigFile = mConfigFile;
        this.mBReader    = new BufferedReader( new FileReader( mConfigFile ) );
        loadConfiguration();
    }




    // -------------------------------------------------------------------------------------------------------------- //

                                    // ----------------------------------- //
                                    // -- get and set for standard keys -- //
                                    // ----------------------------------- //

    // -------------------------------------------------------------------------------------------------------------- //




    /** @return host name defined in configuration file */
    public String getHostName()
    {
        return mConfigMap.get( HOST_NAME );
    }


    /** @return login password defined in configuration file */
    public String getLoginPassword()
    {
        return mConfigMap.get( LOGIN_PASSWORD );
    }


    /** @return login name defined in configuration file */
    public String getLoginName()
    {
        return mConfigMap.get( LOGIN_NAME );
    }


    /** @return logging file defined in configuration file */
    public String getLoggingFilePath()
    {
        return mConfigMap.get( LOGGING_FILE_PATH );
    }


    /** @return path to the functions defined in configuration file */
    public String getFunctionDirectory()
    {
        return mConfigMap.get( FUNCTION_DIRECTORY );
    }


    /** @return path to the directory used by the bot defined in configuration file */
    public String getMainBotDirectory()
    {
        return mConfigMap.get( MAIN_BOT_DIRECTORY );
    }


    /** @return number of threads/processors which can be used for the CommandHandler - normally an int(!) */
    public String getCmdHandlerProcNo() { return mConfigMap.get( CMD_HANDLER_PROC_NO ); }


    /** @param hostName  setter for the host name */
    public void setHostName( String hostName )
    {
        mConfigMap.put( HOST_NAME, hostName );
    }


    /** @param password setter for the login password */
    public void setLoginPassword( String password )
    {
        mConfigMap.put( LOGIN_PASSWORD, password );
    }


    /** @param path setter for the logging file path */
    public void setLoggingFilePath( String path )
    {
        mConfigMap.put( LOGGING_FILE_PATH, path );
    }


    /** @param path setter for the function directory path */
    public void setFunctionDirectory( String path )
    {
        mConfigMap.put( FUNCTION_DIRECTORY, path );
    }


    /** @param path setter for the main bot directory path */
    public void setMainBotDirectory( String path )
    {
        mConfigMap.put( MAIN_BOT_DIRECTORY, path );
    }


    /** @param number the amount of threads used by the command handler */
    public void setCmdHandlerProcNo( int number )
    {
        mConfigMap.put( CMD_HANDLER_PROC_NO, Integer.toString( number ) );
    }

}
