package org.tsbs.test;

import com.github.theholywaffle.teamspeak3.api.wrapper.Channel;
import junit.framework.TestCase;
import org.tsbs.manager.ChannelWrapper;
import org.tsbs.util.ApiUtils;

import java.util.ArrayList;

public class TS3BotChannelListenerTest extends TestCase
{
    private final String[] channelNames = { "" };

    private ArrayList<ChannelWrapper> channelList = new ArrayList<>();
    private ServerConnection serverCon;
    public void setUp() throws Exception
    {
        super.setUp();

        serverCon = new ServerConnection();
        serverCon.getChannels().forEach( ( c ) -> {
            System.out.println( c.getName() );
        } );
        System.out.println( ApiUtils.getChannelByName( serverCon.getChannels(), "Games" ) );

        Channel c = ApiUtils.getSubChannel( serverCon.getChannels(), new String[]{ "League of Legends", "#1", } );
        System.out.println( c );
        System.out.println( c.getTotalClients() );
        serverCon.setChannel( c );
        serverCon.setName( "Seplunker" );

        System.out.println( "finish" );
    }

    public void tearDown() throws Exception
    {

    }

    public void testConnect() throws Exception
    {

    }
}