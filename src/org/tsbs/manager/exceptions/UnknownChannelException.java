package org.tsbs.manager.exceptions;

/**
 * @author falx
 * @version v0.1a
 */
public class UnknownChannelException extends RuntimeException
{
    public UnknownChannelException()
    {
    }

    public UnknownChannelException( String message )
    {
        super( message );
    }

    public UnknownChannelException( String message, Throwable cause )
    {
        super( message, cause );
    }
}
