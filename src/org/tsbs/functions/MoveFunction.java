package org.tsbs.functions;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;
import com.github.theholywaffle.teamspeak3.api.wrapper.Channel;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import org.tsbs.core.CallableFunction;
import org.tsbs.core.TS3BotChannelListener;
import org.tsbs.util.ApiUtils;

import java.util.List;

/**
 *
 * <b>Not Threadsafe!</b>
 *
 * @author falx
 * @version v0.1a
 */
public class MoveFunction implements CallableFunction
{
    private static final String fMOVE_CHANNEL_CLIENTS = "-all";
    private static final String fMOVE_SINGLE_CLIENT   = "-single";

    private static final String USAGE = String.format( "MoveFunction: written by falx\n" +
        "allowed parameter:\n\t %s  \n\t <channelToMoveTo> \nExample: !move -all Diablo3\n", fMOVE_CHANNEL_CLIENTS );


    @Override
    public void callAction(String[] params, TS3BotChannelListener source, TextMessageEvent eventSource)
    {
        if( params.length < 3 )
            return;

        TS3Api api = source.getTS3Api();

        switch( params[1] )
        {
            case fMOVE_CHANNEL_CLIENTS:
                Channel channelSrc = ApiUtils.getChannelById( api.getChannels(), source.getChannelId() );
                moveChannelClients( api, channelSrc, params[2] );
                break;
            default:
                printUsage( api );
                break;
        }
    }

    public void moveChannelClients( TS3Api api, Channel channel, String destChannel )
    {
        List<Client> clientList = ApiUtils.filterClientsFromOtherChannels( api.getClients(), channel );
        Channel dest = api.getChannelByName( destChannel );

        if( dest != null )
        {
            printUsage( api );
            return;
        }

        for( Client c : clientList )
            api.moveClient( c.getId(), dest.getId() );
    }


    public void printUsage( TS3Api api )
    {
        api.sendChannelMessage( USAGE );
    }
}
