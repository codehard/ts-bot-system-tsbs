package org.tsbs.manager.exceptions;

/**
 * project: TS-Bot-System [TSBS]
 * in package org.tsbs.manager.exceptions
 * created on 17.05.2014
 *
 * @author falx
 * @version v0.1a
 */
public class UnknownFunctionException extends RuntimeException
{
    public UnknownFunctionException()
    {
    }

    public UnknownFunctionException( String message )
    {
        super( message );
    }

    public UnknownFunctionException( String message, Throwable cause )
    {
        super( message, cause );
    }
}
