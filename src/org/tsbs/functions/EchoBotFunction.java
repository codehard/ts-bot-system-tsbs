package org.tsbs.functions;

import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;
import org.tsbs.core.CallableFunction;
import org.tsbs.core.TS3BotChannelListener;

/**
 * project: TS-Bot-System [TSBS]
 * in package org.tsbs.functions
 * created on 06.04.2014
 *
 * @author falx
 * @version v0.1a
 */
public class EchoBotFunction implements CallableFunction
{
    public EchoBotFunction()
    {
        /* */
    }


   @Override
   public void callAction( String[] params, TS3BotChannelListener source, TextMessageEvent eventSource )
   {
      source.getTS3Api().sendChannelMessage( params[1] );
   }
}
