package org.tsbs.test;

import com.github.theholywaffle.teamspeak3.api.wrapper.Channel;
import junit.framework.TestCase;
import org.junit.Before;
import org.tsbs.manager.BotConfiguration;
import org.tsbs.manager.BotManager;
import org.tsbs.util.ApiUtils;

import java.io.IOException;
import java.util.List;

public class BotManagerTest extends TestCase
{

    private BotConfiguration config;
    private BotManager manager;

    @Before
    public void setUp() throws Exception
    {
        super.setUp();

        try
        {
            config = new BotConfiguration( "config\\config.cfg" );
        }
        catch ( IOException exc )
        {
            System.out.println( "config failed" );
        }


        manager = BotManager.createBotManager( config );
    }


    public void tearDown() throws Exception
    {

    }

    public void testCreateBotManager() throws Exception
    {
        System.out.println( "create finished\n\n\n" );
        Thread.sleep( 1500L );
    }

    public void testGetAllServerChannels() throws Exception
    {
        List<Channel> list = manager.getAllServerChannels();
        assertEquals( list.size(), 36 );

        Thread.sleep( 1500L );
    }

    public void testGetActiveChannels() throws Exception
    {

        Thread.sleep( 1500L );
    }

    public void testIsChannelActive() throws Exception
    {

        Thread.sleep( 1500L );
    }

    public void testActivateChannel() throws Exception
    {
        Channel c = ApiUtils.getSubChannel( manager.getAllServerChannels(), new String[]{ "Games", "Diablo3", "#1" } );
        Thread.sleep( 1500L );
    }

    public void testActivateChannel1() throws Exception
    {

        Thread.sleep( 1500L );
    }

    public void testDeactivateChannel() throws Exception
    {

        Thread.sleep( 1500L );
    }

    public void testDeactivateChannel1() throws Exception
    {

        Thread.sleep( 1500L );
    }

    public void testGetAllFunctionClasses() throws Exception
    {

        Thread.sleep( 1500L );
    }

    public void testGetAllFunctionNames() throws Exception
    {

        Thread.sleep( 1500L );
    }

    public void testGetAllActiveFunctionNames() throws Exception
    {

        Thread.sleep( 1500L );
    }

    public void testRenameFunction() throws Exception
    {

        Thread.sleep( 1500L );
    }

    public void testSetFunctionActive() throws Exception
    {

        Thread.sleep( 1500L );
    }

    public void testSetFunctionActive1() throws Exception
    {

        Thread.sleep( 1500L );
    }

    public void testSetFunctionInactive() throws Exception
    {

        Thread.sleep( 1500L );
    }

    public void testSetBotActive() throws Exception
    {

        Thread.sleep( 1500L );
    }

    public void testSetBotInactive() throws Exception
    {

        Thread.sleep( 1500L );
    }
}