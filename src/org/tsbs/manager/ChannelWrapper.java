package org.tsbs.manager;

import com.github.theholywaffle.teamspeak3.api.wrapper.Channel;
import org.tsbs.core.TS3BotChannelListener;

/**
 * @author falx
 * @version v0.1a
 */
public class ChannelWrapper
{
    public final Channel mfChannel;
    public final TS3BotChannelListener mfChannelListener;

    private boolean mIsActive;

    /**
     *
     * @param channel the TS3Channel this wrapper refers to
     * @param channelListener the TS3ChannelListener connected to the TS3Channel
     * @param isActive show if the channelListener is active or not
     */
    public ChannelWrapper( Channel channel, TS3BotChannelListener channelListener, boolean isActive )
    {
        if( channel == null || channelListener == null )
            throw new NullPointerException();

        mfChannel         = channel;
        mfChannelListener = channelListener;
        mIsActive         = isActive;

        if( mIsActive )
            setActive();
    }

    /** return true if the channelListener is active, else false (active == connected) */
    public boolean isActive()
    {
        return mIsActive;
    }

    /** activates the channelListener */
    public void setActive()
    {
        mIsActive = true;
    }

    /** deactivates the channelListener */
    public void setInactive()
    {
        mIsActive = false;
    }

    /** sets the channel active and connects the listener */
    public void connect()
    {
        setActive();
        mfChannelListener.connect();
    }

    /** set the channel deactive and disconnects the listener */
    public void disconnect()
    {
        setInactive();
        mfChannelListener.disconnect();
    }



    @Override
    public boolean equals( Object o )
    {
        ChannelWrapper cw;

        if( !(o instanceof ChannelWrapper) )
            return false;
        else
            cw = (ChannelWrapper)o;

        boolean sameChannel  = cw.mfChannel.getId() == this.mfChannel.getId();
        boolean sameListener = this.mfChannelListener.equals( cw );

        return sameChannel & sameListener;
    }

}
