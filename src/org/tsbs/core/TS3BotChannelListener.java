package org.tsbs.core;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.github.theholywaffle.teamspeak3.api.TextMessageTargetMode;
import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;
import org.tsbs.util.TS3ListenerAdapter;

/**
 * project: TS-Bot-System [TSBS]
 * in package org.tsbs.core
 * <p>
 * A TS3BotChannelListener can be set to each channel, where the commands
 * should be usable. Once a channelListener is set to a specific channel
 * it can not be changed anymore!
 * This ChannelListener <b>does not</b> process the commands!
 * <p>
 * TODO: refactor this class, so the channel is not changeable, but the name should be!
 *
 * @author falx
 * @version v0.7a
 * @created 05.04.2014
 */
public class TS3BotChannelListener
{
    // -------------------------------------------------------------------------------------------------------------- //

                                        // ------------------------- //
                                        // -- member declarations -- //
                                        // ------------------------- //

    // -------------------------------------------------------------------------------------------------------------- //
    // Objects to connect to the TS3-Server and to
    // handle the TS3-Query-API
    private TS3Api mApi;
    private TS3Query mQuery;
    private TS3ListenerAdapter mListenerAdapter;

    // Handles the eradication of the requested actions
    // ( e.g. !votekick <clientname> )
    private CommandHandler mCommandHandler;

    private int mChannelId;
    private String mName;




    // -------------------------------------------------------------------------------------------------------------- //

                                            // ------------------ //
                                            // -- constructors -- //
                                            // ------------------ //

    // -------------------------------------------------------------------------------------------------------------- //




    /**
     * Creates a new TS3BotChannelListener. It's connection is based on
     * the TS3Config-object
     *
     * @param config
     *         config object on which the connection is based
     * @param connect
     *         if {@code true} the created object will connect directly {@code else} it doesn't
     * @param commandHandler
     *         the handler this ChannelHandler reports to
     * @param channelId
     *         id of the channel this ChannelListener has to work at.
     */
    public TS3BotChannelListener( TS3Config config, boolean connect, CommandHandler commandHandler, int channelId )
    {
        this( new TS3Query( config ), false, commandHandler, channelId );
    }


    /**
     * Creates new TS3BotChannelListener. It's connection is based on
     * the TS3Query-object
     *
     * @param query
     *         the query object used to connect to the TS3 server
     * @param connect
     *         if {@code true} the created object will connect directly {@code else} it doesn't
     * @param commandHandler
     *         the handler this ChannelHandler reports to
     * @param channelId
     *         id of the channel this ChannelListener has to work at.
     */
    public TS3BotChannelListener( TS3Query query, boolean connect, CommandHandler commandHandler, int channelId )
    {
        this.mQuery = query;
        this.mCommandHandler = commandHandler;
        this.mChannelId = channelId;

        if ( connect )
            connect();
    }




    //----------------------------------------- private methods ----------------------------------------------------- //

    // -------------------------------------------------------------------------------------------------------------- //

                                        // ----------------------- //
                                        // -- initiation method -- //
                                        // ----------------------- //

    // -------------------------------------------------------------------------------------------------------------- //




    private void configure()
    {
        mListenerAdapter = new TS3ListenerAdapter()
        {

            @Override
            public void onTextMessage( TextMessageEvent event )
            {
                if ( event.getTargetMode() == TextMessageTargetMode.CHANNEL && event.getMessage().startsWith( "!" ) )
                {
                    CommandWrapper wrap = new CommandWrapper( event.getMessage(), TS3BotChannelListener.this, event );
                    mCommandHandler.enqueueCommandAction( wrap );
                    System.out.print( "" );
                }
            }
        };

        mApi = mQuery.getApi();
        mApi.selectVirtualServerById( 1 );
        mApi.registerAllEvents();
        mApi.addTS3Listeners( mListenerAdapter );
        mApi.moveClient( mChannelId );

        mName = "Bot " + mChannelId;
        mApi.setNickname( mName );
    }




    //--------------------------------------------- public methods -------------------------------------------------- //





    /**
     * this method connects this ChannelHandler with the TS3-server
     */
    public void connect()
    {
        mQuery.connect();
        configure();
    }


    /**
     * disconnects this ChannelHandler from the current TS3-server
     */
    public void disconnect()
    {
        mQuery.exit();
    }


    /**
     * @return the Id of the channel this ChannelListener is current user of.
     */
    public int getChannelId()
    {
        return mChannelId;
    }


    /**
     * @return the TS3Api this ChannelHandler uses
     */
    public TS3Api getTS3Api()
    {
        return mApi;
    }


}
