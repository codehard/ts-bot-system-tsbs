package org.tsbs.test;

import org.tsbs.core.CallableFunction;
import org.tsbs.manager.BotConfiguration;
import org.tsbs.manager.BotManager;
import org.tsbs.manager.exceptions.ConfigNotFoundException;
import org.tsbs.manager.exceptions.DirectoryNotFoundException;
import org.tsbs.util.ApiUtils;

import java.io.IOException;
import java.util.Map;

/**
 * project: TS-Bot-System [TSBS]
 * in package: org.tsbs.test
 * created on 4/9/14
 *
 * @author falx
 * @version v0.1a.
 */
public class ManagerTest
{
    private static BotConfiguration config;
    private static BotManager manager;

    public static void main( String[] args ) throws ClassNotFoundException, IllegalAccessException, InstantiationException, IOException, DirectoryNotFoundException, ConfigNotFoundException, InterruptedException
    {
        config = new BotConfiguration( "config\\config.cfg" );
        manager = BotManager.createBotManager( config );

        manager.activateChannel( ApiUtils.getSubChannel( manager.getAllServerChannels(), new String[]{ "Games", "Diablo3", "#1" } ) );
        manager.activateChannel( ApiUtils.getSubChannel( manager.getAllServerChannels(), new String[]{ "Games", "League of Legends", "#1" } ) );
        manager.activateChannel( ApiUtils.getSubChannel( manager.getAllServerChannels(), new String[]{ "Sonstiges", "Chillen", "Hangout" } ) );
        Map<String, Class<? extends CallableFunction>> map = manager.getAllFunctionNames();
        for( String s : map.keySet() )
        {
            if( s.equals( "EchoBotFunction" ) )
            {
                manager.setFunctionActive( map.get( s ) );
                manager.renameFunction( map.get(s), "echo" );
                break;
            }
        }

//        manager.setBotActive();

//        Thread.sleep( 25000L );

//        manager.setBotInactive();
//        System.exit(0);
    }
}
