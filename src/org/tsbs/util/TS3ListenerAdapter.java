package org.tsbs.util;

import com.github.theholywaffle.teamspeak3.api.event.*;

/**
 * project: TS-Bot-System [TSBS]
 * in package org.tsbs
 * created on 05.04.2014
 * <p>
 * Adapter-class for the TS3Listener interface.
 * Each method of the TS3Listener interface is implemented,
 * but without any function ( empty method bodies ).
 * This adapter-class can be used to have better/clean code.
 *
 * @author falx
 * @version v0.1a
 */
public class TS3ListenerAdapter implements TS3Listener
{

    @Override
    public void onTextMessage( TextMessageEvent e )
    {

    }

    @Override
    public void onClientJoin( ClientJoinEvent e )
    {

    }

    @Override
    public void onClientLeave( ClientLeaveEvent e )
    {

    }

    @Override
    public void onServerEdit( ServerEditedEvent e )
    {

    }

    @Override
    public void onChannelEdit( ChannelEditedEvent e )
    {

    }

    @Override
    public void onChannelDescriptionChanged( ChannelDescriptionEditedEvent e )
    {

    }

    @Override
    public void onClientMoved( ClientMovedEvent e )
    {

    }
}
