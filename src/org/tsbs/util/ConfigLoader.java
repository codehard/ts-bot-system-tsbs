package org.tsbs.util;

import java.io.*;
import java.util.HashMap;
import java.util.Map;

/**
 * project: ts-bot-system-tsbs
 * in package org.tsbs.test
 * <p>
 * Singelton architecture class to get a ConfigLoader object.
 * Such an object contains a Map with config data.
 * <pre>
 * Possible keys (- values ):
 *    host     - hostname of the ts server
 *    name     - login name for the ts-query
 *    password - login password for the ts-query
 * </pre>
 *
 * @author falx
 * @version v0.2a
 * @created 04.04.2014
 */
public class ConfigLoader
{
    // static block //

    private static ConfigLoader configLoader;
    private static final String configUrl = "config\\config.cfg";

    public final HashMap<String, String> configMap;

    private ConfigLoader() throws FileNotFoundException, IOException
    {
        configMap = new HashMap<>();
        File configFile = new File( configUrl );

        try ( BufferedReader br = new BufferedReader( new FileReader( configFile ) ); )
        {
            String input;
            while ( ( input = br.readLine() ) != null )
            {
                String[] pair = input.split( ":" );
                configMap.put( pair[0], pair[1] );
            }
        }
        catch ( FileNotFoundException exception )
        {
            System.err.println( exception ); // FIXME
            throw exception;
        }
        catch ( IOException exception )
        {
            System.err.println( exception ); // FIXME
            throw exception;
        }

        configMap.putAll( System.getenv() );
    }

    /**
     * !! <b>NOT threadsafe!</b>
     *
     * @return configLoader
     * containing a map with conifg data
     *
     * @throws IOException
     *         if loading the config file and / or reading it failed
     */
    public static ConfigLoader getConfigLoader() throws IOException
    {
        if ( configLoader == null )
        {
            configLoader = new ConfigLoader();
        }

        return configLoader;
    }


    public static Map<String, String> loadConfigMap( String path ) throws FileNotFoundException, IOException
    {
        File file = new File( path );
        HashMap<String, String> configMap = new HashMap<>();

        if ( !file.exists() )
            throw new FileNotFoundException( "Configfile which was tried to load not found!" );

        try ( BufferedReader br = new BufferedReader( new FileReader( file ) ); )
        {
            String input;
            while ( ( input = br.readLine() ) != null )
            {
                String[] pair = input.split( ":" );
                configMap.put( pair[0], pair[1] );
            }
        }
        catch ( FileNotFoundException exception )
        {
            System.err.println( exception ); // FIXME
            throw exception;
        }
        catch ( IOException exception )
        {
            System.err.println( exception ); // FIXME
            throw exception;
        }

        return configMap;
    }
}
