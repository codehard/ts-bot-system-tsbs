package org.tsbs.core;

import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;

import java.util.HashMap;
import java.util.Map;
import java.util.concurrent.ExecutorService;
import java.util.concurrent.Executors;

/**
 * project: TS-Bot-System [TSBS]
 * in package org.tsbs.core
 * <p>
 * </p>
 * A CommandHandler - object can process Commands. Therefore it is using
 * a {@see java.util.concurrent.Executors#newFixedThreadPool} which is running
 * CommandEngineWorker objects.
 *
 * @author falx
 * @version v0.3a
 * @created 05.04.2014
 */
public class CommandHandler
{
    private ExecutorService mThreadPool;
    private HashMap<String, CallableFunction> mCommandMap;
    private volatile boolean mIsRunning;
    private int mPoolSize;


    // constructor method(s) //

    /**
     * Creates a new CommandHandler but does not start the ExecutorService
     * until run() is called!
     *
     * @param poolSize
     *         size of the ExecutorPool
     * @param commandMap
     *         contains the usable commands for the bot
     */
    public CommandHandler( int poolSize, HashMap<String, CallableFunction> commandMap )
    {
        this.mPoolSize = poolSize;
        this.mCommandMap = new HashMap<>();

        this.mCommandMap.putAll( commandMap );
    }

    /**
     * Creates a new CommandHandler using the given pool-size and an empty HashMap.
     * The CommandHandler will not start until run() is called!
     *
     * @param poolSize
     *         size of the ExecutorPool
     */
    public CommandHandler( int poolSize )
    {
        this( poolSize, new HashMap<>() );
    }

    // public method(s) //

    /**
     * Starts the CommandHandler and its ExecutorService
     */
    public synchronized void run()
    {
        if ( mIsRunning )
            throw new IllegalStateException( "CommandHandler is already running!" );

        mThreadPool = Executors.newFixedThreadPool( mPoolSize );
        mIsRunning = true;
    }

    /**
     * Stops the CommandHandler and its ExecutorService.
     * <b>This method is only supported if the CommandHandler is NOT running!</b>
     * <pre><b><u>Important:</u>
     *  This method calls the shutdownNow()-method of the ExecutorService!</b></pre>
     */
    public synchronized void stop()
    {
        if ( !mIsRunning )
            throw new IllegalStateException( "CommandHandler is not running!" );
        mThreadPool.shutdownNow();

        mIsRunning = false;
    }

    /**
     * @return if the CommandHandler is running {@code true else false}
     */
    public boolean isRunning()
    {
        return mIsRunning;
    }


    /**
     * <b>This method is only supported if the CommandHandler is NOT running!</b>
     *
     * @param functionMap
     *         adds the function with the specific command set
     */
    public void addFunctions( HashMap<String, CallableFunction> functionMap )
    {
        if ( mIsRunning )
            throw new IllegalStateException( "Method not available while the CommandHandler is running!" );

        mCommandMap.putAll( functionMap );
    }


    /**
     * Adds a new instance of a command to the CommandHandler-queue
     *
     * @param commandWrapper
     *         containing the data needed for process the command
     */
    public void enqueueCommandAction( CommandWrapper commandWrapper )
    {
        mThreadPool.execute(  new CommandEngineWorker( commandWrapper ) );
    }


    /**
     * @return a HashMap containing all used Functions including their commands
     */
    public HashMap<String, CallableFunction> getCommandMap()
    {
        return new HashMap<String, CallableFunction>( mCommandMap );
    }


    /**
     * <b>This method is only supported if the CommandHandler is NOT running!</b>
     *
     * @param map
     *         overrides all existing functions used by this commandHandler
     */
    public synchronized void overrideFunctions( Map<String, CallableFunction> map )
    {
        if ( mIsRunning )
            throw new IllegalStateException( "This operation is not supported while the CommandHandler is running!" );
        mCommandMap = new HashMap<>( map );
    }


    // command engine implemented as worker //

    /**
     * private inner class:
     * Represents an Runnable-object used by the CommandHandler-class to process
     * a requested command
     *
     * @author falx
     * @version v0.3a
     */
    private class CommandEngineWorker implements Runnable
    {
        private final TS3BotChannelListener mfSourceListener;
        private final String mfCommand;
        private final TextMessageEvent mfSourceEvent;

        /**
         * @param commandWrapper
         *         specifing the command to run
         */
        public CommandEngineWorker( CommandWrapper commandWrapper )
        {
            this.mfSourceListener = commandWrapper.fSourceChannelListener;
            this.mfCommand = commandWrapper.fCommand.trim();
            this.mfSourceEvent = commandWrapper.fSourceTextEvent;
        }



        @Override
        public void run()
        {
            String tmpCommand = mfCommand.substring( mfCommand.indexOf( "!" ) + 1 );
            String[] params = tmpCommand.split( " " );

            CallableFunction function = mCommandMap.get( params[0] );
            if ( function != null )
                function.callAction( params, mfSourceListener, mfSourceEvent );
        }
    }
}
