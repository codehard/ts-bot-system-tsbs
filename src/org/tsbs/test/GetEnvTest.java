package org.tsbs.test;

/**
 * project: TS-Bot-System [TSBS]
 * in package: org.tsbs.test
 * created on 4/9/14
 *
 * @author falx
 * @version v0.1a.
 */
public class GetEnvTest
{
    public static void main( String[] args )
    {
        for( String s : System.getenv().keySet() )
            System.out.println( s + "\t" + System.getenv( s ) );

        System.out.println( System.getProperty( "os.name" ) );
    }
}
