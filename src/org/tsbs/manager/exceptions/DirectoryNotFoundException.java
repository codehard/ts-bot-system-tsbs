package org.tsbs.manager.exceptions;

/**
 * Created by Kristoffer on 13.04.2014.
 */
public class DirectoryNotFoundException extends Exception
{
    public DirectoryNotFoundException()
    {
        super();
    }

    public DirectoryNotFoundException( String msg )
    {
        super( msg );
    }

    public DirectoryNotFoundException( String msg, Throwable cause )
    {
        super( msg, cause );
    }
}
