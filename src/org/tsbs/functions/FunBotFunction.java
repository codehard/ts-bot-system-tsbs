package org.tsbs.functions;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;
import com.github.theholywaffle.teamspeak3.api.wrapper.Channel;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import org.tsbs.core.CallableFunction;
import org.tsbs.core.TS3BotChannelListener;
import org.tsbs.util.ApiUtils;

import java.util.List;
import java.util.Random;

/**
 * project: TS-Bot-System [TSBS]
 * in package org.tsbs.functions
 * created on 06.04.2014
 *
 * @author falx
 * @version v0.1a
 */
public class FunBotFunction implements CallableFunction
{
    public FunBotFunction()
    {
        /* */
    }


   @Override
   public void callAction( String[] params, TS3BotChannelListener source, TextMessageEvent eventSource )
   {
      TS3Api api = source.getTS3Api();
      Random random = new Random();
      List<Channel> channelList = ApiUtils.filterPasswordProtectedChannels( api.getChannels() );
      Channel sourceChannel =  ApiUtils.getChannelById( channelList, source.getChannelId() );
      List<Client>  clientList  = ApiUtils.filterClientsFromOtherChannels( api.getClients(), sourceChannel );
      Channel channel = channelList.get( random.nextInt( channelList.size() ) );
      Client  client  = clientList.get( random.nextInt( clientList.size() ) );

      if( params.length > 1 )
         api.moveClient( eventSource.getInvokerId(), channel.getId() );
      else
         api.moveClient( client.getId(), channel.getId() );
   }



}
