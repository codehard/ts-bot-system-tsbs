package org.tsbs.core;

import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;

/**
 * @author falx
 * @verions v0.3a
 */
public class CommandWrapper
{
    public final String fCommand;
    public final String[] fParams;
    public final TS3BotChannelListener fSourceChannelListener;
    public final TextMessageEvent fSourceTextEvent;

    /**
     *
     * @param command
     * @param sourceChannelListener
     * @param sourceTextEvent
     *
     */
    public CommandWrapper( String command, TS3BotChannelListener sourceChannelListener, TextMessageEvent sourceTextEvent )
    {
        this.fCommand = command;
        this.fParams  = setUpParams( command );
        this.fSourceChannelListener = sourceChannelListener;
        this.fSourceTextEvent = sourceTextEvent;
    }

    private String[] setUpParams( String commandStr )
    {
        commandStr.trim();
        commandStr = commandStr.substring( commandStr.indexOf( "!" ) );
        return commandStr.split( " " );
    }
}
