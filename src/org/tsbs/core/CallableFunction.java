package org.tsbs.core;

import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;

/**
 * project: TS-Bot-System [TSBS]
 * in package org.tsbs.core
 * <p>
 * Basic interface for actions based on TS3Events.
 * So each CommandAction has to implement this interface!
 *
 * @author falx
 * @version v0.1a
 * @created 05.04.2014
 */

@FunctionalInterface
public interface CallableFunction
{
    /**
     * @param params
     *         contains the !<nameoffunction> on param[0] and else parameters for the function
     * @param source
     *         source ChannelListener
     * @param eventSource
     *         event which caused the call
     */
    void callAction( String[] params, TS3BotChannelListener source, TextMessageEvent eventSource );
}
