package org.tsbs.test;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;
import com.github.theholywaffle.teamspeak3.api.wrapper.Channel;
import org.tsbs.core.CallableFunction;
import org.tsbs.core.CommandHandler;
import org.tsbs.core.TS3BotChannelListener;
import org.tsbs.functions.EchoBotFunction;
import org.tsbs.functions.FunBotFunction;
import org.tsbs.functions.MoveFunction;
import org.tsbs.manager.BotConfiguration;
import org.tsbs.util.ConfigLoader;
import org.tsbs.util.TS3ListenerAdapter;

import java.io.IOException;
import java.util.HashMap;
import java.util.List;
import java.util.logging.Level;

/**
 * project: TS-Bot-System [TSBS]
 * in package org.tsbs.test
 * created on 06.04.2014
 *
 * @author falx
 * @version v0.1a
 */
public class Test
{
    public static void main( String[] args ) throws IOException
    {
        HashMap<String, String> configMap = ConfigLoader.getConfigLoader().configMap;
        int i = 0;

        TS3Config config1 = new TS3Config();
        config1.setHost( configMap.get( "host" ) );
        config1.setDebugLevel( Level.OFF );
        config1.setDebugToFile( true );
        config1.setLoginCredentials( configMap.get( "loginnname" ), configMap.get( "loginpassword" ) );
        config1.setFloodRate( TS3Query.FloodRate.DEFAULT );

        TS3Query query = new TS3Query( config1 );
        query.connect();
        TS3Api api = query.getApi();
        api.selectVirtualServerById( 1 );
        api.setNickname( "test" );
        // api.sendChannelMessage( "test" );
        api.registerAllEvents();
        api.addTS3Listeners( new TS3ListenerAdapter()
        {
            @Override
            public void onTextMessage( TextMessageEvent e )
            {
                System.err.println( "hey!" );
            }
        } );
        System.err.println( "test" );

        List<Channel> l = api.getChannels();
        query.exit();

        HashMap<String, CallableFunction> map = new HashMap<>();
        map.put( "!echo", new EchoBotFunction() );
        map.put( "!fun", new FunBotFunction() );
        map.put( "!move", new MoveFunction() );
        CommandHandler handler = new CommandHandler( 1, map );
        handler.run();


        BotConfiguration conf = new BotConfiguration( "config\\config.cfg" );
        TS3Config config2 = new TS3Config();
        config2.setHost( configMap.get( "host" ) );
        config2.setDebugLevel( Level.ALL );
        config2.setDebugToFile( true );
        config2.setLoginCredentials(conf.getLoginName(), conf.getLoginPassword() );
        config2.setFloodRate( TS3Query.FloodRate.DEFAULT );

        TS3Query query1 = new TS3Query( config2 );
        TS3BotChannelListener bot1 = new TS3BotChannelListener( query1, true, handler, 26 );



    }
}
