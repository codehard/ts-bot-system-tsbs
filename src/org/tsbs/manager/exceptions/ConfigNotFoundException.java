package org.tsbs.manager.exceptions;

/**
 * Created by Kristoffer on 12.04.2014.
 */
public class ConfigNotFoundException extends Exception
{
    public ConfigNotFoundException( String msg )
    {
        super( msg );
    }

    public ConfigNotFoundException()
    {
        super();
    }
}
