package org.tsbs.test;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.github.theholywaffle.teamspeak3.api.wrapper.Channel;
import org.tsbs.manager.BotConfiguration;
import org.tsbs.util.ApiUtils;

import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.List;
import java.util.logging.Level;

/**
 * @author falx
 * @version v0.1a
 */
public class ServerConnection
{
    private BotConfiguration mConfig;
    private TS3Query mQuery;
    private TS3Api mApi;


    public ServerConnection() throws FileNotFoundException, IOException
    {
        this.mConfig = new BotConfiguration( "config/config.cfg" );
        this.mQuery  = new TS3Query( getConfig() );
        this.mQuery.connect();
        this.mApi    = this.mQuery.getApi();
        this.mApi.selectVirtualServerById( 1 );
    }


    public TS3Config getConfig()
    {
        TS3Config cfg = new TS3Config();
        cfg.setHost( this.mConfig.getHostName() );
        cfg.setLoginCredentials( this.mConfig.getLoginName(), this.mConfig.getLoginPassword() );
        cfg.setDebugLevel( Level.ALL );

        return cfg;
    }


    public void setName( String name )
    {
        mApi.setNickname( name );
    }

    public void sendMessage( String msg )
    {
        mApi.sendChannelMessage( msg );
    }

    public void setChannel( Channel c )
    {
        mApi.moveClient( c.getId() );
    }


    public List<Channel> getChannels()
    {
        return mApi.getChannels();
    }

    public Channel getChannelByName( String name )
    {
        return ApiUtils.getChannelByName( getChannels(), name );
    }

}
