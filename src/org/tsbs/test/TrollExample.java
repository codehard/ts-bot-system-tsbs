package org.tsbs.test;

import com.github.theholywaffle.teamspeak3.TS3Api;
import com.github.theholywaffle.teamspeak3.TS3Config;
import com.github.theholywaffle.teamspeak3.TS3Query;
import com.github.theholywaffle.teamspeak3.api.event.*;
import com.github.theholywaffle.teamspeak3.api.wrapper.Channel;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;
import org.tsbs.util.ConfigLoader;

import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Random;
import java.util.logging.Level;


/**
 * project: TS-Bot-System [TSBS]
 * in package org.tsbs.test
 *
 * @author Bert De Geyter modified by falx
 *         source http://github.com/TheHolyWaffle/TeamSpeak-3-Java-API/blob/master/src/main/java/com/github/theholywaffle/teamspeak3/example/ChatBotExample.java
 */

public class TrollExample
{

    public static void main( String[] args ) throws IOException
    {
        newTroll();
        newTroll();
    }

    public static void newTroll() throws IOException
    {
        final TS3Config config = new TS3Config();
        final HashMap<String, String> configMap = ConfigLoader.getConfigLoader().configMap;

        config.setHost( configMap.get( "host" ) );
        config.setDebugLevel( Level.ALL );
        config.setDebugToFile( true );
        config.setLoginCredentials( configMap.get( "queryname" ), configMap.get( "querypassword" ) );
        config.setFloodRate( TS3Query.FloodRate.DEFAULT );

        final TS3Query query = new TS3Query( config );
        query.connect();

        final TS3Api api = query.getApi();
        api.selectVirtualServerById( 1 );
        api.setNickname( "PutPutBot" );
        System.out.println( "send now!" );
        if( api.getChannels() == null )
            System.err.println( "null" );


        api.registerAllEvents();
        api.addTS3Listeners( new TS3Listener()
        {
            @Override
            public void onTextMessage( TextMessageEvent e )
            {
                System.out.println( e.getMessage() );
            }

            @Override
            public void onClientJoin( ClientJoinEvent e )
            {

            }

            @Override
            public void onClientLeave( ClientLeaveEvent e )
            {

            }

            @Override
            public void onServerEdit( ServerEditedEvent e )
            {

            }

            @Override
            public void onChannelEdit( ChannelEditedEvent e )
            {

            }

            @Override
            public void onChannelDescriptionChanged( ChannelDescriptionEditedEvent e )
            {

            }

            @Override
            public void onClientMoved( ClientMovedEvent e )
            {

            }
        } );

        System.out.println( api.moveClient( searchForChannel( "Lernen", api.getChannels() ).getId() ) );
        api.sendChannelMessage( "PutPutBot is online!" );

    }

    public static void troll( String name, TS3Api bot )
    {
        ArrayList<Channel> channels = new ArrayList<>();
        for( Channel c : bot.getChannels() )
        {
            if( c.getTotalClients() > 0 )
            {
                channels.add( c );
            }
            System.out.println( c.getName() );
        }

        int clientId = bot.getClientByName( name ).get( 0 ).getId();

        Random r = new Random();
        while( true )
        {
            int channelId = channels.get( r.nextInt( channels.size() ) ).getId();
            bot.moveClient( clientId, channelId );
            try
            {
                Thread.sleep( 2000 );
            } catch( InterruptedException e )
            {
                e.printStackTrace();
            }
        }
    }

    public static void troll2( String name, TS3Api bot )
    {
        int clientId = bot.getClientByName( name ).get( 0 ).getId();

        int i = 0;
        Random r = new Random();
        while( true )
        {
            try
            {
                Thread.sleep( r.nextInt( 30_000 ) + 30_000 );
            } catch( InterruptedException e )
            {
                e.printStackTrace();
            }
            bot.pokeClient( clientId, i + "" );
            i++;
        }
    }

    public static Channel searchForChannel( String name, List<Channel> channelList )
    {
        for( Channel c : channelList )
        {
            if( c.getName().equals( name ) )
                return c;
        }
        return null;
    }

    private static Client searchForClient( String name, List<Client> clientList )
    {
        for( Client c : clientList )
            if( c.getNickname().equals( name ) )
            {
                System.err.println( c.getNickname() );
                return c;
            }

        return null;
    }

}