package org.tsbs.manager;

import org.tsbs.core.CallableFunction;

import java.io.File;

/**
 * project: TS-Bot-System [TSBS]
 * in package: org.tsbs
 *
 * Simple wrapper class for functions.
 * It stores the File object to the class file where it
 * is loaded from, an instance of the CallableAction, defined
 * in the class file and the used name to call this function /
 * CallableAction.
 *
 * @author falx
 * @version v0.1a.
 * @created 09.04.2014
 */
public class FunctionWrapper
{
    private final Class<? extends CallableFunction> fCallableFunction;
    private final File fFunctionFile;
    private String mFunctionName;
    private boolean mIsActive;

    /**
     *
     * @param callableAction an instance of the CallableAction defined in the funcition file
     * @param functionFile File object referencing to the class file where callableAction is defined in
     * @param functionName the name used to call this function later
     * @param isActive indicates if this Function is currently in use or not
     */
    public FunctionWrapper( Class<? extends CallableFunction> callableAction, File functionFile, String functionName, boolean isActive)
    {
        this.fCallableFunction = callableAction;
        this.fFunctionFile     = functionFile;
        this.mFunctionName     = functionName;
        this.mIsActive         = isActive;
    }

    /**
     *
     * @return the save CallableAction instance
     */
    public Class<? extends CallableFunction> getCallableAction()
    {
        return fCallableFunction;
    }

    /**
     *
     * @return the saved File-object, which is referring to the class file
     */
    public File getFunctionFile()
    {
        return fFunctionFile;
    }

    /**
     *
     * @return the current name used to call the saved CallableAction instance
     */
    public String getFunctionName()
    {
        return mFunctionName;
    }

    /**
     *
     * @return true if this function is currently in use, else false
     */
    public boolean isActive()
    {
        return mIsActive;
    }

    /**
     *
     * @param name the new name for this function/ callableAction. /! not null && size() > 0 !/
     */
    public void setFunctionName( String name )
    {
        if( name == null || name.length() == 0 )
            throw new RuntimeException( "Name must be not null and needs at least one character!" );

        mFunctionName = name;
    }

    /**
     *
     * @param isActive the state of active or not active of this function
     */
    public void setIsActive( boolean isActive )
    {
        this.mIsActive = isActive;
    }


    @Override
    public boolean equals( Object o )
    {
        FunctionWrapper wrapper;

        if( !(o instanceof FunctionWrapper ) )
            return false;
        else
            wrapper = (FunctionWrapper)o;

        return wrapper.fFunctionFile.equals( this.fFunctionFile );
    }
}
