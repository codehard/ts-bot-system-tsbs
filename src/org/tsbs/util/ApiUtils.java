package org.tsbs.util;

import com.github.theholywaffle.teamspeak3.api.wrapper.Channel;
import com.github.theholywaffle.teamspeak3.api.wrapper.Client;

import java.util.List;

/**
 * project: TS-Bot-System [TSBS]
 * in package org.tsbs.util
 *
 * @author falx
 * @version v0.1a
 * @created 06.04.2014
 */
public class ApiUtils
{
    /**
     * Searches for the channel with the channelid id
     *
     * @param list
     *         a channelList where to search for the channel
     * @param id
     *         id of the channel to search
     *
     * @return a channel-object with the channelId id or null if not found
     */
    public static Channel getChannelById( List<Channel> list, int id )
    {
        for ( Channel c : list )
            if ( c.getId() == id )
                return c;

        return null;
    }

    /**
     * Searches for the channel with the channelName name
     *
     * @param list
     *         a channelList where to search a channelList where to search for the channel
     * @param name
     *         name of the channel to search
     *
     * @return a channel-object with the channelName name or null if not found
     */
    public static Channel getChannelByName( List<Channel> list, String name )
    {
        for ( Channel c : list )
            if ( c.getName().equals( name ) )
                return c;

        return null;
    }

    /**
     * Filters out every client who is not in the channel channel
     *
     * @param list
     *         a clientList which should be filtered
     * @param channel
     *         the channel which every client should be in to not get filtered out
     *
     * @return a client-list containing only clients which are currently in Channel channel
     */
    public static List<Client> filterClientsFromOtherChannels( List<Client> list, Channel channel )
    {
        list.removeIf( ( t ) -> {
            return t.getChannelId() != channel.getId();
        } );
        return list;
    }


    /**
     * Filters out every channel which is password protected
     *
     * @param list
     *         a channelList to be filtered out
     *
     * @return a channelList only containing non-password protected channels
     */
    public static List<Channel> filterPasswordProtectedChannels( List<Channel> list )
    {
        list.removeIf( ( t ) -> {
            return t.hasPassword();
        } );
        return list;
    }


    /**
     * <pre>
     *     ---------
     *     | games | ===> node (hierarchy[0])
     *     ---------   -------
     *         |-----> | LoL | ===> some other channel (hierarchy[1])
     *                 -------    ------
     *                    |-----> | #1 | ===> the channel we're searching (hierarchy[length-1])
     *                            ------
     * </pre>
     *
     * @param list
     *         a channelList where to search for the channel
     * @param hierarchy
     *         contains the Channel names in hierarchy form: first element = node, last element = channel searched
     *
     * @return the channel if found, else null.
     */
    public static Channel getSubChannel( List<Channel> list, String[] hierarchy )
    {
        Channel p = getChannelByName( list, hierarchy[0] );

        for ( int i = 1; p != null && i < hierarchy.length; i++ )
            p = getChannelByNameAndPId( list, hierarchy[i], p.getId() );

        return p;
    }


    /**
     * @param list
     *         a channelList where to search for the channel
     * @param name
     *         the name of the channel which is searched
     * @param pid
     *         the id of the searched channel parent
     *
     * @return the channel if found, else null.
     */
    public static Channel getChannelByNameAndPId( List<Channel> list, String name, int pid )
    {
        for ( Channel c : list )
            if ( c.getName().equals( name ) && c.getParentChannelId() == pid )
                return c;

        return null;
    }


    /**
     * @param list
     *         a channelList to be filtered out
     *
     * @return a list of channels only containing channels which are joinable
     */
    public static List<Channel> filterNotJoinableChannels( List<Channel> list )
    {
        list.removeIf( ( t ) -> {
            return t.getMaxClients() == 0;
        } );
        return list;
    }


}
