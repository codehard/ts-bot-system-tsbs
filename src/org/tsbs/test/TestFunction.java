package org.tsbs.test;

import com.github.theholywaffle.teamspeak3.api.event.TextMessageEvent;
import org.tsbs.core.CallableFunction;
import org.tsbs.core.TS3BotChannelListener;

/**
 * @author falx
 * @version v0.1a
 */
public class TestFunction implements CallableFunction
{

    public void callAction( String[] params, TS3BotChannelListener source, TextMessageEvent eventSource )
    {
        source.getTS3Api().sendChannelMessage( "testings..." );
    }

}
